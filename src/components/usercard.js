import React from "react";

const UserCard = ({ user , length}) => {

    return (
        <div className="user_card" style = {{maxWidth:length > 2 ?'89px':'123px'}}>
            <img src={user.img} alt={user.name} style={{width:"100%"}} />
            <p className = "name_user">{user.name}</p>
        </div>

    )
};

export default UserCard