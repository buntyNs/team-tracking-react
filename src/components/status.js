import React from 'react'

const Status = (props) => {
    var  element  = null;
    switch(props.status){
        case "DISPATCHED":return element = <h2 style = {{color : '#007bff'}}>ON THE WAY</h2>
        break;
        case "IN_PROGRESS":return element =  <h2 style = {{color : '#17a2b8'}}>WORK IN PROGRESS</h2>
        break;
        case "COMPLETED": return element =  <h2 style = {{color : '#28a745'}}>COMPLETED</h2>
        break;
        default : element =  <h2 style = {{color : '#28a745'}}>INITIAL</h2>;
        
    }
  
  return (
   {element}
  );
};

export default Status;