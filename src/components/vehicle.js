import React from 'react'

const Vehicle = (props) => {
    var teamIcon = null;
    switch (props.type) {
        case "BIKE": teamIcon = "https://img.icons8.com/ios-glyphs/40/000000/motorcycle.png"
            break;
        case "CAR": teamIcon = "https://img.icons8.com/ios-glyphs/40/000000/people-in-car-side-view.png";
            break;
        case "LORRY": teamIcon = "https://img.icons8.com/pastel-glyph/40/000000/courier-lorry.png";
            break;
        case "THREE_WHEELER": teamIcon = "https://img.icons8.com/ios-filled/40/000000/three-wheel-car.png";
            break;
        case "VAN": teamIcon = "https://img.icons8.com/ios-filled/40/000000/van.png";
            break;
        case "WALKING": teamIcon = "https://img.icons8.com/ios-filled/40/000000/walking.png";
            break;
        default: teamIcon = "https://img.icons8.com/ios-filled/40/000000/walking.png";;
    }
    return (
        <img src = {teamIcon} style={{ height: 40, width: 40, margin: "10px"}} ></img>
    );
};

export default Vehicle;