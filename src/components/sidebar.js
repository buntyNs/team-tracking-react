import React from "react";
import { FaWindowClose, FaMobileAlt, FaDotCircle, FaShuttleVan, FaMotorcycle, FaTruck } from 'react-icons/fa';
import Slider from "react-slick";
import UserCard from './usercard';
import StarRatings from 'react-star-ratings';
import { Row, Col } from 'reactstrap';
import Status from './status';
import Vehicle from "./vehicle";
import { Spinner } from 'reactstrap';


const getContact = (list) => {
  var splitList = list.split(",");
  return splitList[0];
}

const Sidebar = (props) => {
  if (props.teamDetails != null) {
    var settings = {
      speed: 500,
      slidesToShow: props.teamDetails.Members.length,
      slidesToScroll: props.teamDetails.Members.length,
    };
    var contact = getContact(props.teamDetails.ContactNo);
  }

  return (

    <div className={props.sidebarOpen ? "open_left" : "close_left"}>
      <div>
        <section className="section_close">
          <FaWindowClose style={{ float: "right", margin: "10px", color: "black", height: "15px", width: '15px' }} onClick={props.close} />
        </section>
        {(props.teamDetails == null || props.orderDetails == null) ? <Spinner style={{ position: 'absolute', left: '38%', top: '45%', width: '70px', height: '70px' }} color="warning" type="grow" /> :
          <div>
            <section className="section_rating" >
              <StarRatings
                rating={3}
                starRatedColor="#ffc107"
                changeRating={this.changeRating}
                numberOfStars={5}
                name='rating'
                starDimension="40px"
                starSpacing="2px"
                style={{ margin: '10px' }}
              />
            </section>

            <section className="section_vehicle">
              <Vehicle type={props.teamDetails.VehicleType}></Vehicle>
              <Status status={props.jobStatus}></Status>
            </section>

            <Row className="contact">
              <Col><FaMobileAlt style={{ height: 20, width: 20, color: '#00bbd7' }}></FaMobileAlt></Col>
              <Col> <h4>Contact</h4></Col>
              <Col> <h4>{contact}</h4></Col>
            </Row>

            <section className="section_member">
              <Slider {...settings}>
                {
                  props.teamDetails.Members.map((member,index) => {
                    return (
                      <UserCard key = {index} length = {props.teamDetails.Members.length}  user={{ name: member.Name, img: member.Image != null ? member.Image : 'https://www.schoolchalao.com/app/webroot/img/no-user-image.png' }} />
                    );
                  })
                }
              </Slider>
            </section>

            <section className="order_details">
              <span className="order_title">Order Details</span>
            </section>

            <div style={{ marginTop: "10px", backgroundColor: "white", margin: 0, padding: '15px', fontSize: "12px", display: 'grid' }}>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4"><span>Order Id</span></Col><Col xs="6"><span>: {props.orderDetails.OrderId}</span></Col></Row>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4" ><span>Acount No</span></Col><Col xs="6"><span>: {props.orderDetails.AccountNo}</span></Col></Row>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4" ><span>Cx Name</span></Col><Col xs="6"><span>: {props.orderDetails.CustomerName}</span></Col></Row>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4" ><span>Address</span></Col><Col xs="6"><span>: {props.orderDetails.Address}</span></Col></Row>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4" ><span>Req Date</span></Col><Col xs="6"><span>: {props.orderDetails.RequestDate}</span></Col></Row>
              <Row style = {{padding:'10px'}}> <Col xs="1" ><FaDotCircle /></Col><Col xs="4" ><span>Unit</span></Col><Col xs="6"><span>: {props.orderDetails.WoBisUnit}</span></Col></Row>
            </div>

          </div>

        }
      </div>
    </div>

  )
};

export default Sidebar