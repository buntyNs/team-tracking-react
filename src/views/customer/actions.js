
const actions = {
 
  openSideBar: (pmsId) => ({
    type: "OPEN_SIDEBAR",
    payload: pmsId
  }),
  closeSideBar: () => ({ type: "CLOSE_SIDEBAR" }),

  getTeamLocation: (location) => ({
    type: "GET_TEAM_LOCATION",
    payload: location
  }),
  getCustomerLocation: (location) => ({
    type: "GET_CUSTOMER_LOCATION",
    payload: location
  }),

  setPath: (path) => ({
    type: "SET_PATH",
    payload: path
  })
  ,
  setDirection: (direction) => ({
    type: "GET_DIRECTION",
    payload: direction
  })
  ,
  setJobStatus: (status) => ({
    type: "SET_JOB_STATUS",
    payload: status
  })
,
  setOrderDetails: (order) => ({
    type: "GET_ORDER_DETAILS",
    payload: order
  })
,
  setTeamDetails: (team) => ({
    type: "SET_TEAM_DETAILS",
    payload: team
  })
};

export default actions;
