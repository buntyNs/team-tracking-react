import React, { Component } from 'react';
import {
    withGoogleMap,
    GoogleMap,
    Marker,
    withScriptjs,
} from "react-google-maps";
import MapDirectionsRenderer from './mapdireaction';
import { connect } from 'react-redux';
import Actions from '../actions';
import TeamMarker from './teammarker';

class Map extends React.Component {
    getInfo = (pmsId) => {
        this.props.dispatch(Actions.openSideBar(pmsId));
    }

    render() {
        return (
            <GoogleMap
                options={{ gestureHandling: "greedy" }}
            >
                {
                    this.props.teamLocation != null ? <TeamMarker
                        teamLocation = {this.props.teamLocation}
                        getInfo = {this.getInfo}
                        type = {this.props.VehicleType}
                    >
                    </TeamMarker> : null
                }
                {
                    this.props.customerLocation != null ?
                <Marker
                    position={{ lat: parseFloat(this.props.customerLocation.latitude), lng: parseFloat(this.props.customerLocation.longitude) }}
                    icon={"https://img.icons8.com/ios-glyphs/40/000000/user-location.png"}
                ></Marker>
                :null
                }
                <MapDirectionsRenderer
                    travelMode={window.google.maps.TravelMode.DRIVING}
                />
            </GoogleMap>
        )
    }

}

export default connect()(withScriptjs(withGoogleMap(Map)))
