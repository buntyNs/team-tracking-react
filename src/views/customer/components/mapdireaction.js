/* global google */
import React from "react";
import {
  DirectionsRenderer
} from "react-google-maps";
import { connect } from 'react-redux';
import Actions from '../actions';
class MapDirectionsRenderer extends React.Component {
    constructor(props){
        super(props)
    }

  loadRoute(places,travelMode){
    const waypoints = places.map(p =>({
      location: {lat: p.latitude, lng:p.longitude},
      stopover: true
  }))

  const origin = waypoints.shift().location;
  const destination = waypoints.pop().location;
  
  

  const directionsService = new google.maps.DirectionsService();
  directionsService.route(
    {
      origin: origin,
      destination: destination,
      travelMode: travelMode,
      waypoints: waypoints,
    },
    (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
          this.props.dispatch(Actions.setDirection(result));
      } else {
      }
    }
  );
  }

  componentDidMount() {
    const { path, travelMode } = this.props;
    if(path.length > 0){
      this.loadRoute(path,travelMode);
    }
    
  }

  render() {
    const { path, travelMode } = this.props;
    if(path.length > 0){
      this.loadRoute(path,travelMode);
    }
    return (this.props.directions && <DirectionsRenderer 
      options = {{
      suppressMarkers:true,
      polylineOptions: { 
        strokeColor: 'black',
        strokeOpacity: 1.0,
        strokeWeight: 5
     }
    }} 
   
    directions={this.props.directions} />)
  }
}

const mapStateToProps = (state) => {
    return {
        directions : state.customer.directions,
        path : state.customer.path

    }
  };

export default connect(mapStateToProps)(MapDirectionsRenderer);