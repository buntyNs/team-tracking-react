
export default (state = {
  sidebarOpen: false,
  loader: false,
  pmsId: "",
  teamLocation: null,
  customerLocation: null,
  path: [],
  directions: null,
  jobStatus : "DISPATCHED",
  orderDetails :null,
  teamDetails : null
}, action) => {
  switch (action.type) {
    case "OPEN_SIDEBAR":
      return { ...state, sidebarOpen: true, pmsId: action.payload }
    case "CLOSE_SIDEBAR":
      return { ...state, sidebarOpen: false, pmsId: "" }
    case "GET_TEAM_LOCATION":
      return { ...state, teamLocation: action.payload }
    case "GET_CUSTOMER_LOCATION":
      return { ...state, customerLocation: action.payload }
    case "SET_PATH":
      return { ...state, path: action.payload }
    case "GET_DIRECTION":
      return { ...state, directions: action.payload }
    case "SET_JOB_STATUS": 
    return { ...state,jobStatus: action.payload }
    case "GET_ORDER_DETAILS": 
    return { ...state,orderDetails: action.payload } 
    case "SET_TEAM_DETAILS": 
    return { ...state,teamDetails: action.payload } 

    default: return state;
  }
};
