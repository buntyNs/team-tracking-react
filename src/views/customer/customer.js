import React, { Component } from 'react';
import Map from './components/map';
import Sidebar from '../../components/sidebar';
import Actions from './actions'
import './customer.css';
import Network from '../../utils/network';
import axios from 'axios';
import { Spinner } from 'reactstrap';



class Customer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getOrderDetails(this.props.match.params.jobId);
    this.getTeamDetails(this.props.match.params.pmsId);
    this.getJobStatus(this.props.match.params.jobId);
    axios.post(`http://172.26.28.126:8081/api/v1/team/location`, [this.props.match.params.pmsId])
      .then(response => {
        if (response.data.length > 0 && this.props.customerLocation != null ) {
          const path = [
            this.props.customerLocation, response.data[0]
          ]
          this.props.dispatch(Actions.setPath(path));
        } else {
          this.props.dispatch(Actions.setPath([]));
        }
        this.props.dispatch(Actions.getTeamLocation(response.data[0]));

      });


    this.intervalId = setInterval(() => { this.getTeamLocation(this.props.match.params.pmsId) }, 10000);
    this.teamIntervalId = setInterval(() => { this.getJobStatus(this.props.match.params.jobId) }, 10000);
  }


  componentWillUnmount() {
    clearInterval(this.intervalId);
    clearInterval(this.teamIntervalId);
  }


  getTeamLocation(pmsId) {
    axios.post(`http://172.26.28.126:8081/api/v1/team/location`, [pmsId])
      .then(response => {
        if (response.data.length > 0) {
          const path = [
            this.props.customerLocation, response.data[0]
          ]
          this.props.dispatch(Actions.setPath(path));
        } else {
          this.props.dispatch(Actions.setPath([]));
        }
        this.props.dispatch(Actions.getTeamLocation(response.data[0]));
      })
  }

  getJobStatus(jobId){
    axios.get(`http://172.26.76.118:8080/v1/JobSummary?jobId=`+jobId)
      .then(response => {
        this.props.dispatch(Actions.setJobStatus(response.data.data.JobDetails.Status))
      })
  }

  getOrderDetails(jobId){
    axios.get(`http://172.26.76.118:8080/v1/JobSummary?jobId=`+jobId)
      .then(response => {
        this.props.dispatch(Actions.setOrderDetails(response.data.data.JobDetails));
        this.props.dispatch(Actions.getCustomerLocation({
          "longitude": response.data.data.JobDetails.Location.Longitude,
          "latitude": response.data.data.JobDetails.Location.Latitude
        }));
      })
  }

  getTeamDetails(pmsId){
    axios.get(`http://172.26.76.118:8080/v1/WomTeamProfile?PmsId=`+pmsId)
      .then(response => {
        this.props.dispatch(Actions.setTeamDetails(response.data.data))
      })
  }

  closeSideBar = ()=>{
    this.props.dispatch(Actions.closeSideBar());
  }


  render() {

    return (

      <div>
        {(this.props.teamLocation == null || this.props.customerLocation == null || this.props.path.length == 0) ? <Spinner style={{ position: 'absolute', left: '48%', top: '45%', width: '70px', height: '70px' }} color="warning" type="grow" />:
        <div>
        <Sidebar 
        sidebarOpen={this.props.sidebarOpen}
        close = {this.closeSideBar}
        teamDetails = {this.props.teamDetails}
        orderDetails = {this.props.orderDetails}
        jobStatus = {this.props.jobStatus}
        ></Sidebar>
        <Map
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDUC-vnqkz0_0VMqXjlOA2yaJUFToCJk&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `100vh` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          teamLocation={this.props.teamLocation}
          customerLocation={this.props.customerLocation}
          VehicleType = {this.props.teamDetails != null ?this.props.teamDetails.VehicleType:null}
        >
        </Map>
        </div>
        }
      </div>
 
    );
  }
}

export default Customer;