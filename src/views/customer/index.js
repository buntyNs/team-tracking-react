import { connect } from 'react-redux';
import Customer from './customer';

const mapStateToProps = (state) => {
  return {
    sidebarOpen  : state.customer.sidebarOpen,
    teamLocation : state.customer.teamLocation,
    customerLocation : state.customer.customerLocation,
    teamDetails : state.customer.teamDetails,
    orderDetails : state.customer.orderDetails,
    jobStatus : state.customer.jobStatus,
    path : state.customer.path
    
  }
};

export default connect(mapStateToProps)(Customer);