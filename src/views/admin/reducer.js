
export default (state = {
  pmsId: "",
  team : [],
  sidebarOpen : false
}, action) => {
  switch (action.type) {
    case "OPEN_SIDEBAR":
      return { ...state, sidebarOpen: true, pmsId: action.payload }
    case "CLOSE_SIDEBAR":
      return { ...state, sidebarOpen: false, pmsId: "" }
    case "GET_ALL_TEAM":
      return {...state, team:action.payload}  
    default: return state;
  }
};
