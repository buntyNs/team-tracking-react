import React, { Component } from 'react';
import Actions from './actions'
import Sidebar from '../../components/sidebar';
import axios from 'axios';
import Map from './components/map';

class Admin extends Component {
  
  constructor(props) {
    super(props);
  }

  closeSideBar = () => {
    this.props.dispatch(Actions.closeSideBar());
  }
  
  componentDidMount(){
    axios.get('http://localhost:8080/api/v1/team')
    .then((response) => {
      this.props.dispatch(Actions.getAllTeams(response.data));
    })
    .catch((error) => {
      console.log(error);
    })

    this.intervalId = setInterval(this.getTeamLocations.bind(this), 10000);

  }


  getTeamLocations() {
    // if (this.props.pmsId == "") {
      
    // } else {
    //   const pmsId = [
    //     parseInt(this.props.pmsId)
    //   ];
    //   axios.post(`http://localhost:8080/api/v1/team/team_location`, pmsId)
    //     .then(response => {
    //       this.props.dispatch(Actions.getAllTeams(response.data));
    //     })
    // }

    axios.get('http://localhost:8080/api/v1/team')
        .then((response) => {
          this.props.dispatch(Actions.getAllTeams(response.data));
        })
        .catch((error) => {
          console.log(error);
        })

  }

  render () {
    return (
      <div>
      <Sidebar sidebarOpen={this.props.sidebarOpen} close = {this.closeSideBar}></Sidebar>
      <Map
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsDUC-vnqkz0_0VMqXjlOA2yaJUFToCJk&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100vh` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      >
      </Map>
    </div>
    );
  }
}

export default Admin;