import React from 'react';
import {
    Marker

} from "react-google-maps";
import { connect } from 'react-redux';
import Actions from '../../admin/actions';


class Markers extends React.Component {
    getInfo = (pmsId)=>{
        this.props.dispatch(Actions.openSideBar(pmsId));
    }
    render() {
        return this.props.team.map(place => {
            return (
                <Marker
                    key={place.profileId}
                    position={{ lat: parseFloat(place.latitude), lng: parseFloat(place.longitude) }}
                    onClick = {()=>{this.getInfo(place.profileId)}}
                    icon = {"https://img.icons8.com/ios-glyphs/40/000000/marker.png"}
                >
                </Marker>
            );
        });
    }
}

const mapStateToProps = (state) => {
    return {
        team : state.admin.team
    }
  };

export default connect(mapStateToProps)(Markers)
