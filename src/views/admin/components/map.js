import React from 'react';
import {
    withGoogleMap,
    GoogleMap,
    withScriptjs,
} from "react-google-maps";
import Markers from './markers';
import { connect } from 'react-redux';
import Actions from '../actions';



class Map extends React.Component {

    getInfo = (pmsId)=>{
        this.props.dispatch(Actions.openSideBar(pmsId));
    }

    render() {
        return (
            <GoogleMap
                options={{ gestureHandling: "greedy" }}
                zoom = {8}
                defaultCenter={{lat:6.929856,lng:79.895198}}
            >
                <Markers></Markers>
            </GoogleMap>
        )
    }

}

export default connect()(withScriptjs(withGoogleMap(Map)))
