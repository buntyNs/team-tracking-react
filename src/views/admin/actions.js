
const actions = {
  getAllTeams: (team) => {
    return {
      type: "GET_ALL_TEAM",
      payload: team
    }
  },

  openSideBar: (pmsId) => ({
    type: "OPEN_SIDEBAR",
    payload: pmsId
  }),
  closeSideBar: () => ({ type: "CLOSE_SIDEBAR" }),



};

export default actions;
