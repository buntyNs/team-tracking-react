import { connect } from 'react-redux';
import Admin from './admin';

const mapStateToProps = (state) => {
  return {
    sidebarOpen : state.admin.sidebarOpen,
    pmsId :state.admin.pmsId
  }
};

export default connect(mapStateToProps)(Admin);